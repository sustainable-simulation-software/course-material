# Development of a simulation environment

In this project, we want to develop an environment for solving the following problem over a general domain $\Omega \text{ with boundary } \Gamma$ and unit outer normal vector $\mathbf{n}$:

```math
\nabla \cdot \left( - \Phi \nabla u \right) = q \quad \mathrm{in} \, \Omega \\
\left( - \Phi \nabla u \right) \cdot \mathbf{n} = F_N \quad \mathrm{on} \, \Gamma_N \\
u = u_D \quad \mathrm{on} \, \Gamma_D,
```

where $u$ is the unknown and $\Phi \text{ and } q$ are scalar functions. The second and third equations constitute the boundary conditions, where $F_N \text{ and } u_D$ refer to the boundary values at Neumann and Dirichlet boundaries, respectively.
We want to develop a software framework that allows users to solve this problem on arbitrary domains, while providing
the means to define $\Phi, \, q, \, F_N \text{ and } u_D$ anywhere in the domain or on the boundary, respectively.
In order to solve this problem, we want to employ a cell-centered finite volume discretization scheme using
two-point flux approximation, which is briefly introduced in the following.

## Discretization scheme

Let us consider a discretization $\mathcal{M} \text{ of the domain } \Omega$, consisting of cells $E \in \mathcal{M} \text{ such that } \Omega \approx \Omega_h = \bigcup_{E \in \mathcal{M}} E$.
We now want the above equation to be fulfilled in each cell (control volume) of the mesh. To this end, we integrate the equation over a grid cell:

$$\int_E \nabla \cdot \left( - \Phi \nabla u \right) \, \mathrm{d}x = \int_E q \, \mathrm{d}x,$$

and apply the Gauss divergence theorem to arrive at

$$\int_{\partial E} \left( - \Phi \nabla u \right) \cdot \mathbf{n} \, \mathrm{d}x = \int_E q \, \mathrm{d}x.$$

Here, $\partial E$ denotes the boundary of the grid cell $E \text{ and } \mathbf{n}$ is the unit outer normal vector on $\partial E$. Let us approximate $q$ with a piecewise-constant (per cell) function $q_E$ (note: if we use $q_E = 1/|E| \int_E q \mathrm{d}x$, we recover the original term), and rewrite the right-hand side to

$$\int_{\partial E} \left( - \Phi \nabla u \right) \cdot \mathbf{n} \, \mathrm{d}x = q_E |E|,$$

where $|E|$ denotes the measure of the cell, that is, its length (1D), area (in 2D) or volume (in 3D). Let us now introduce with $\mathcal{S}_E \text{ the set of faces of } E$ that compose its boundary. This allows us to rewrite the left-hand-side of the equation into a sum over all faces of the cell:

$$\sum_{\sigma \in \mathcal{S}_E} \int_{\sigma} \left( - \Phi \nabla u \right) \cdot \mathbf{n} \, \mathrm{d}x = q_E |E|.$$

Finally, we introduce the discrete flux $F_\sigma \approx \int_{\sigma} - \left( \Phi \nabla u \right) \cdot \mathbf{n} \, \mathrm{d}x \text{ across a face } \sigma \in \mathcal{S}_E$ to arrive at the final formulation:

$$\sum_{\sigma \in \mathcal{S}_E} F_\sigma = q_E |E|.$$

The above equation states that the sum over all fluxes across the faces of a grid cell have to balance out with the sources in the interior of the cell. See the below figure for a graphical illustration of a grid cell and its faces.

<img src="discretization.svg" width="500px"/>

### Flux expression

What is still missing is an expression for the discrete flux $F_\sigma$.
In this project, we will use a simple two-point flux approximation which only involves information of the two adjacent cells of a face.
We introduce one degree of freedom per cell, denoted with $u_E$ and associated with the cell center,
and use piecewise-constant (per cell) $\Phi_E$.
Moreover, let us introduce the vector

$$\mathbf{d}^E_{\sigma} = \mathbf{x}_{\sigma} - \mathbf{x}_E, E \in \mathcal{M}, \sigma \in \mathcal{S}_{E}.$$

Note that here, $\mathbf{x}_\sigma \text{ and } \mathbf{x}_E$ denote the coordinates of the centers of the face and the cell.
With these definitions one can derive the two-point flux expression for the flux across the face $\sigma \text{ as seen from the cell } E$ as

$$F_\sigma^E = - \left( u_\sigma - u_E \right) \left[ \Phi_E \left(\mathbf{d}_\sigma^E \cdot \mathbf{n} \right) |\sigma| / |d_\sigma^E|^2 \right].$$

For more details on the derivation, see e.g. [The DuMuX Documentation](https://dumux.org/docs/doxygen/master/group___c_c_tpfa_discretization.html), section 3.3.1.
With the definition of the half-transmissibility

$$t_{\sigma}^E = - \Phi_E \left(\mathbf{d}_\sigma^E \cdot \mathbf{n} \right) |\sigma| / |d_\sigma^E|^2$$

this reduces to

$$F_\sigma^E = t_{\sigma}^E \left( u_\sigma - u_E \right)$$

Let us consider a configuration as depicted in the image above, and assume that the face $\sigma$ lies in between the two cells $E \text{ and } K$, while the normal vector on the face points outside of $E$.
In this setting, we can express the flux across $\sigma$ as seen from the "outside" cell $K$ as

$$F_\sigma^K = - t_{\sigma}^K \left( u_\sigma - u_K \right),$$

where the inverted sign comes from the normal vector pointing inside the cell. Enforcing flux continuity up to the sign across the face, that is, $F_\sigma^E = - F_\sigma^K$,
we get an expression for $u_\sigma$:

$$u_{\sigma} = \left( t_\sigma^E u_E - t_{\sigma}^K u_K \right) / \left( t_\sigma^E - t_\sigma^K \right).$$

Plugging this back into the expression for $F_\sigma^E$, we get:

$$F_\sigma^E = t_{\sigma}^E \left[ \left( t_\sigma^E u_E - t_{\sigma}^K u_K \right) / \left( t_\sigma^E - t_\sigma^K \right) - u_E \right] = t_{\sigma}^E \left( t_\sigma^K u_E - t_{\sigma}^K u_K \right) / \left( t_\sigma^E - t_\sigma^K \right) = t_{\sigma}^E t_{\sigma}^K / \left( t_\sigma^E - t_\sigma^K \right) \left( u_E - u_K \right).$$```

Finally, definining the transmissibility $t_\sigma = t_{\sigma}^E t_{\sigma}^K / \left( t_\sigma^E - t_\sigma^K \right)$, the final flux expression reduces to

$$F_\sigma^E = t_\sigma \left( u_E - u_K \right).$$

### Boundary conditions

Both Dirichlet and Neumann boundary conditions are incorporated weakly, that is, for Dirichlet conditions one simply sets $u_\sigma = u_D$
in the flux expression $F_\sigma^E = t_{\sigma}^E \left( u_\sigma - u_E \right)$
and computes the corresponding discrete flux.
On Neumann boundaries, $F_\sigma^E$ is substituted with $F_N |\sigma|$.

### Assembly into a linear system

Requiring the discrete equation to be fulfilled in each grid cell results in a linear system of the form

```math
\mathbf{A} \mathbf{u} = \mathbf{b},
```

where $\mathbf{A}$ is a square matrix with a size equal to the number of cells of the discretization, and $\mathbf{u}$ is the solution vector holding the cell-wise constant values $u_E$, $E \in \mathcal{M}$.
The right-hand side $\mathbf{b}$ contains the source terms as well as terms from the boundary conditions that do not depend on cell values $u_E$.
The discrete equation for a cell only depends on itself and the immediate neighbors, and therefore, the matrix $\mathbf{A}$ is sparse.

## Requirements on the software

To solve the problem, we need the ingredients to assemble, for a given _grid_ and _boundary conditions_, the linear system which we can then plug
into an existing linear solver library (e.g. `scipy.sparse`). We want to collect the functionality for assembling and solving the system in a
`solver` module. This solver should be interoperable with different types of grids (2d/3d, structured, unstructured, ...), and thus, it should
operate on an abstract grid interface which is generic enough such that different types of grids can be implemented behind it. Moreover, it should
expose a convenient way for a user to specify boundary conditions and parameters. The solver module should not provide grid implementations, these
should be developed as a separate `grid` module. For more details on the scope of the solver module, see the template repository at
[gitlab.com/sustainable-simulation-software/group-work-templates/solver](https://gitlab.com/sustainable-simulation-software/group-work-templates/solver).

The `grid` module should provide implementations of different grids (structured, unstructured, 2d/3d), ideally, all behind the same interface.
The discrete equation above gives us insights on what functionality we need, and for a full overview see the template repository at
[gitlab.com/sustainable-simulation-software/group-work-templates/grid](https://gitlab.com/sustainable-simulation-software/group-work-templates/grid).
As can be seen in the above discrete equation, access to geometric properties of grid faces and cells is required. The `grid` module should not
implement the computation of these properties, but instead, a separate `geometry` module should be developed, which provides implementations of
geometric primitives. For a full overview of the required functionalities, see the template repository at
[gitlab.com/sustainable-simulation-software/group-work-templates/geometry](https://gitlab.com/sustainable-simulation-software/group-work-templates/geometry).

As can be seen from the description above, the project has the following dependency hierarchy:

<img src="project_uml.svg" width="200px"/>


## Goals

### 1: Match an analytical solution on a square two-dimensional grid

To test the implementation, we can use an analytical solution, for instance

```math
u = \mathrm{sin}(x) \mathrm{cos}(y)\\
q = 2 \Phi \mathrm{sin}(x) \mathrm{cos}(y)
```

on a two-dimensional domain with square or rectangular grid cells and $\Phi = const$.
The image below visualizes the solution in the domain on the left side, together with the exact solution plotted along the diagonal on the right side.

<img src="analytical_solution.png" width="700px"/>


### 2: Unstructured grids

The implementation should be tested on unstructured grids, for instance, using arbitrary quadrilaterals or triangular grids. To this end, the mesh generator
[gmsh](https://gmsh.info/) can be used, while appropriate I/O routines for reading gmsh mesh files have to be implemented. An example using the analytical solution
from before can be seen in the figure below.

<img src="analytical_solution_unstructured.png" width="700px"/>


### 3: Heterogeneous $\Phi$

The implementation should be tested for heterogeneous parameter fields. The image below shows an example for

```math
u_D = \mathrm{sin}(x) \mathrm{cos}(y)\\
q = 0 \\
\Phi = \{ 1 \, \mathrm{if} \, x < 0.5; \, 2 \, \mathrm{else} \}
```

on the unit square.

<img src="heterogeneous_domain.png" width="700px"/>


### 4: Three-dimensional grids

Bringing all the pieces together, three-dimensional grids should also work:

<img src="three_dimensional_solution.png" width="700px"/>
