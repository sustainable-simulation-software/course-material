import os
import sys
import shutil
from argparse import ArgumentParser


def is_source_file(filename):
    return os.path.basename(filename) == "index.html"


def revealjs_path_substitutor(rel_path):
    def substitute(content):
        return content.replace(
            "[[PATH_TO_REVEALJS]]",
            rel_path
        )
    return substitute


def get_revealjs_rel_path(file_in_build_dir, build_dir) -> str:
    rel_path_to_build = os.path.relpath(
        build_dir,
        os.path.dirname(file_in_build_dir)
    )
    rel_path_to_reveal = os.path.join(rel_path_to_build, "reveal.js")
    return rel_path_to_reveal


def substitute_and_write(source, target, substitutor):
    raw_content = open(source, "r").read()
    with open(target, 'w') as target_file:
        target_file.write(substitutor(raw_content))


def copy_folders(source, target):
    shutil.copytree(source, target, dirs_exist_ok=True)


def process_source_file(source, target, build_folder):
    revjs_rel_path = get_revealjs_rel_path(target, build_folder)
    substitute_and_write(
        source,
        target,
        revealjs_path_substitutor(revjs_rel_path)
    )


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Compile the slide templates into a reveal.js presentation."
    )
    parser.add_argument(
        "-b", "--buildfolder",
        required=False, default="__build",
        help="Set the folder in which to put the built slides"
    )
    parser.add_argument(
        "-r", "--revealjsfolder",
        required=False, default="reveal.js",
        help="Specify the path to reveal.js"
    )
    args = vars(parser.parse_args())

    build_folder = args["buildfolder"]
    reveal_folder = args["revealjsfolder"]

    # prerequisites
    cwd = os.getcwd()
    if cwd != os.path.dirname(os.path.abspath(__file__)):
        sys.stderr.write("Must be run from the top folder of the course\n")
        sys.exit(1)
    if not os.path.exists(reveal_folder):
        sys.stderr.write("Reveal.js not found\n")
        sys.exit(1)
    if not os.path.exists(os.path.join(cwd, "index.html")):
        sys.stderr.write("Could not find landing slides\n")
        sys.exit(1)

    # prepare build folder
    if os.path.exists(build_folder):
        shutil.rmtree(build_folder)
    os.makedirs(build_folder, exist_ok=True)

    # copy revealjs & customizations
    shutil.copytree(reveal_folder,
                    os.path.join(build_folder, "reveal.js"),
                    dirs_exist_ok=True,
                    ignore=shutil.ignore_patterns(".git"))
    shutil.copytree(os.path.join(cwd, "dist"),
                    os.path.join(build_folder, "reveal.js/dist"),
                    dirs_exist_ok=True)

    if os.path.isdir("img"):
        copy_folders("img", os.path.join(build_folder, "img"))
    copy_folders("slides", os.path.join(build_folder, "slides"))

    for root, _, files in os.walk("slides"):
        root = os.path.join(cwd, root)
        for source_file in filter(is_source_file, files):
            source_file = os.path.join(root, source_file)
            source_file_rel = os.path.relpath(source_file, cwd)
            process_source_file(
                source_file,
                os.path.join(build_folder, source_file_rel),
                build_folder
            )

    # process landing slides
    source_file = os.path.join(cwd, "index.html")
    build_source_file = os.path.join(build_folder, "index.html")
    process_source_file(
        source_file, build_source_file, build_folder
    )
