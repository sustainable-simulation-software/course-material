#!/bin/bash

# This script will execute the `configure.py` and `make.py` script for setting up the environment for reveal.js. On top of that,
# once building is done, the existing index.html files within all the subfolders will be converted to pdf format and stored into index.pdf
# within each subfolder. After this script has been executed successfully, you can run the script `.create_single_pdf.sh` to concatenate
# all individual `index.pdf` files into one single pdf file. You will need to install decktape, nodejs and chromium-browser for Linux.

BUILD_FOLDER="__build"
SLIDE_FOLDER="__build/slides"

# Make sure there is a `configure.py` file. The configure script will create the necessary, local reveal.js folder which contains all necessary styles and configurations
if [ ! -f "configure.py" ]
then
	echo "configure.py does NOT exist. Terminating." 
else
	echo "Found configure.py."
fi

# Make sure there is a `make.py` file. The make script will create the `__build` folder. Within this folder all paths are complete in contrast to the paths in the `slide folder`
if [ ! -f "make.py" ]
then
	echo "make.py does NOT exist. Terminating." 
else
	echo "Found make.py".
fi

# If the __build folder does not exist yet, run the configure and make script
if [ ! -d "__build" ]
then
	echo "__build folder does NOT exist yet."
	rm -rf reveal.js # In case there are any leftover (local) installations, remove them
	python3 configure.py
	python3 make.py 
else
	echo "Found __build folder.".
fi


# Create a disclaimer front-page for the final PDF
cat <<EOF > __build/intro_slide.html
<!DOCTYPE html>
<html>
	<head>
	    <title>Intro Slide</title>
	    <style>
	        body { font-family: Arial, sans-serif; text-align: center; margin-top: 200px; }
	        h1 { font-size: 48px }
       	  p { font-size: 24px; max-width: 800px; margin: 0 auto;}
       	  a { font-size: 24px; max-width: 800px; margin: 0 auto;}
	    </style>
	</head>
	<body>
	    <h1>This is a PDF version of the online presentation</h1>
	    <p>During the conversion to PDF format, a few functionalities have been restricted. Among other minor issues, code blocks are not scrollable. For the full functionality and experience, please visit the online presentation.</p>
	     <a href="https://sustainable-simulation-software.gitlab.io/course-material/#/title-slide">Click here to access the online presentation</a> 
	</body>
</html>
EOF
# Convert disclaimer intro page to PDF format
decktape --chrome-path chromium-browser automatic __build/intro_slide.html __build/intro_slide.pdf


# Iterate over all subfolders of the `__build` directory (here up until depth 3) and convert all found `index.html` files to `index.pdf` files. Folders ending with "reveal.js" are ignored
for slideFolder in $(find $BUILD_FOLDER -mindepth 0 -maxdepth 3 -type d | grep -v "/reveal.js")
do
	echo "Found slide folder: $slideFolder"

	#check if the index.html files in the subfolders exist.
  if [ ! -f "$slideFolder/index.html" ]
  then
    echo "$slideFolder/index.html does NOT exist."
  else
    echo "Found $slideFolder/index.html."

    # Run Decktape on the HTML file to convert it to pdf format
    # Need to install decktape and chromium browser for this to work. Can also provide the absolute path to chromium browser for --chrome-path, if this one does not work
    decktape --chrome-path chromium-browser reveal $slideFolder/index.html "$slideFolder/index.pdf"
  fi
  echo
done


# This script requires the installation of poppler-utils (for using pdfunite) and concatenates the given pdf files into one single file.
pdfunite \
	__build/intro_slide.pdf \
	__build/index.pdf \
	__build/slides/motivation/index.pdf \
	__build/slides/python_intro/index.pdf \
	__build/slides/git_intro/index.pdf \
	__build/slides/git_intro/basics/index.pdf \
	__build/slides/git_intro/workflows/index.pdf \
	__build/slides/git_intro/ci/index.pdf \
	__build/slides/git_intro/links/index.pdf \
	__build/slides/testing/index.pdf \
	__build/slides/clean_code/index.pdf \
	__build/slides/design_principles/index.pdf \
	__build/slides/design_patterns/index.pdf \
	__build/slides/jupyter/index.pdf \
	__build/slides/project_description/index.pdf \
	__build/combined_pdf.pdf